##########
Utilidades
##########

En este menú, encontramos una serie de herramientas que complementan las utilidades de aGIS ya descritas en capitulos anteriores.

.. figure::  images//05_utilidades/image0.png
   :align:   center
   :width: 30%


Inventario
**********

Esta herramienta permite visualizar los equipos de red después de pulsar sobre “Inventario”. Los datos que devuelve están divididos por capas y son independientes. Estos datos los podemos seleccionar de forma parcial por polígono o en general.

.. figure::  images/05_utilidades/image3_1.png
   :align:   center
   :width: 80%

   Exportación general

Para el cálculo de inventario parcial accederemos a la herramienta de dibujar polígonos del mapa y seleccionaremos la parte deseada a inventariar.

.. figure::  images/05_utilidades/image4_1.png
   :align:   center
   :width: 80%

   Exportación por polígono

Mediante el botón "Gestor de precios" podemos asignar valores a nuestra red para realziar una tasación. Para ello, una vez dentro de Gestor de Precios iremos asignados precios a uestro inventario, mediante el formulario de "Nuevo Precio".

.. figure::  images/05_utilidades/image4_3.png
   :align:   center
   :width: 80%

   Tabla de precios

.. figure::  images/05_utilidades/image4_2.png
   :align:   center
   :width: 80%

   Formulario Nuevo Precio


Una vez configurada la tabla de precios y calculado el inventario clicamos en el botón "Calcular Precios".

.. figure::  images/05_utilidades/image4_4.png
   :align:   center
   :width: 80%

Gestor de Exportación
*********************

Esta herramienta permite la exportación tanto por selección parcial como de forma general, para ello, vamos a Utilidades -> Exportación.

El display que se muestra en pantalla es sencillo y de fácil comprensión. Se muestra una zona superior en la que se muestra un mapa. En la parte inferior, se encuentra la zona donde elegimos los datos a exportar.

Para exportar los datos de forma general, se debe seleccionar una capa y el formato en el que se quiere exportar y, por último, pulsar sobre EXPORTAR.

.. figure::  images/05_utilidades/image1.png
   :align:   center
   :width: 80%

.. |image1_1| image:: images/05_utilidades/image1_1.png
    :width: 25

Si deseamos exportar solo una zona del mapa, se puede seleccionar con la herramienta de polígonos |image1_1|, la sección deseada:

.. figure::  images/05_utilidades/image2.png
   :align:   center
   :width: 80%

Posteriormente, elegimos la capa, el formato y le damos al botón EXPORTAR.


Gestor Documental
*****************

Con esta herramienta, podemos subir archivos de distinta índole a la plataforma, permitiendo exportar los ya existentes en formato CSV, XLS, PDF o incluso imprimirlo. A la hora de crear un nuevo documento, debemos darle al botón añadir y rellenar un formulario que se compone de:

**Nombre:** Nombre que se le quiere poner al documento en aGIS.

**Capa:** El documento en cuestión.

**Elemento:** Elemento a asignar.

**Archivo:** El documento en cuestión.

.. figure::  images/05_utilidades/image13.png
   :align:   center
   :width: 80%

.. |image16| image:: images/05_utilidades/image16.png
       :width: 25

Una vez guardamos, tenemos varias opciones: ver el archivo, pulsando sobre Ver o borrarlo con |image16|.

.. figure::  images/05_utilidades/image14.png
   :align:   center
   :width: 80%

Una vez hecho esto, si vamos al elemento en el mapa y le hacemos clic, en la pestaña "Documentos" aparece el documento que hemos añadido, permitiendo la opción de verlo.

.. figure::  images/05_utilidades/image18.png
   :align:   center
   :width: 80%


Gestor de Trabajos
******************

En el gestor de trabajo, se pueden observar y administrar los distintos trabajos que se han de realizar sobre la red. Para crear un nuevo trabajo, se ha de pulsar sobre la opción "Nuevo Trabajo" y rellenar el formulario que se solicita:

**Nombre:**
    Nombre que se le desea poner al trabajo.

**Descripción:**
    En caso de desear introducir alguna descripción u observación al trabajo.

**Estado:**
    Situación en la que se encuentra el trabajo.

**Capa:**
    Capa en la cual se encuentra el elemento que queremos seleccionar.

**Elemento:**
    Elemento en cuestión sobre el que ha de realizarse el trabajo.

**Activar notificaciones:**
    De manera predeterminada, el que genera el trabajo recibe notificaciones, pero adicionalmente, si marcamos esta casilla y rellenamos con otro correo, también éste recibirá las notificaciones.


Para guardar el trabajo, se ha de hacer clic sobre guardar.

.. figure::  images/05_utilidades/image19.png
   :align:   center
   :width: 80%

   Formulario Trabajos

.. figure::  images/05_utilidades/image20.png
   :align:   center
   :width: 80%

   Tabla Trabajos


Como vemos en la segunda captura, al guardar nos aparece una tabla donde se muestran todos los trabajos con información relevante. Si hacemos clic en alguno de los  que aparece en la última columna de la fila podremos:

.. |image22| image:: images/05_utilidades/image22.png
       :width: 25
.. |image23| image:: images/05_utilidades/image23.png
       :width: 25

**Visualizar** |image22|
    Nos muestra tres columnas en las que se encuentrann de izquierda a derecha: Detalles, Resolución y Documentos. Las dos últimas columnas nos permiten añadir tanto la resolución al trabajo en modo de texto como asignar documentación sobre la misma.

.. figure::  images/05_utilidades/image21.png
   :align:   center
   :width: 80%

**Editar** |image23|
    Permite editar los datos del trabajo.

**Eliminar** |image16|
    Permite borrar el trabajo.


Gestor de Incidencias
*********************

El gestor de incidencias permite administrar las incidencias relacionadas con la red, exportarlas a diferentes formatos o imprimirlas. Para crear una nueva incidencia, hay que hacer clic sobre "Nueva Incidencia" y rellenar el formulario que se solicita.

**Nombre:**
    Nombre que se le desea poner a la incidencia.

**Descripción:**
    En caso de desear introducir alguna descripción u observación a la incidencia.

**Estado:**
    Situación en la que se encuentra la incidencia.

**Capa:**
    Capa en la cual se encuentra el elemento que queremos seleccionar.

**Elemento:**
    Elemento en cuestión sobre el que está la incidencia.

**Activar notificaciones:**
    De manera predeterminada, el que genera la incidencia recibe notificaciones, pero adicionalmente, si marcamos esta casilla y rellenamos con otro correo, también éste recibirá las notificaciones.


Para guardar la incidencia, se ha de hacer clic sobre "guardar".


.. figure::  images/05_utilidades/image24.png
   :align:   center
   :width: 80%

   Formulario Incidencias

.. figure::  images/05_utilidades/image25.png
   :align:   center
   :width: 80%

   Tabla Incidencias

Como vemos en la segunda captura, al guardar nos aparece una tabla donde se muestran todas las incidencias con información relevante. Si hacemos clic en la última columna de la fila "Acción" podremos:

Ver |image22|
    Nos muestra tres columnas en las que se encuentra de izquierda a derecha: Detalles, datos sobre la incidencia, Resolución y Documentos. Las dos últimas columnas nos permiten añadir tanto la resolución a la incidencia en modo de texto como asignar documentación sobre la misma.

.. figure::  images/05_utilidades/image26.png
   :align:   center
   :width: 80%

**Editar** |image23|
    Permite editar los datos de la incidencia.

**Eliminar** |image16|
    Permite borrar la incidencia.