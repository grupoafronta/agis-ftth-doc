########################
Funcionalidades del Mapa
########################

El acceso al mapa principal (núcleo o core de la aplicación) es muy sencillo y accesible gracias al menú lateral que nos permite, desde cualquier lugar dentro de la aplicación, acceder clicando en el botón "Mapa".


.. figure::  images/03_funcionalidades/image1.png
   :align:   center
   :width: 40%


Una vez dentro, podemos comprobar que se despliegan más opciones que examinaremos más adelante.


Mapa
****

.. figure::  images/03_funcionalidades/image2_1.png
   :align:   center
   :width: 80%

   Vista Mapa

Se permite visualizar y acceder a las distintas herramientas y utilidades que permiten interactuar con todo el contenido de la plataforma. La plataforma se compone, principalmente, de dos componentes, un Mapa Base, el cual se puede intercambiar entre los que dispone la plataforma, y un conjunto de capas que se sobreponen al Mapa Base.


Mapa base
=========
Una vez dentro del mapa existe una serie de herramientas básicas:


**1. Zoom**

Zoom nos permite acercar o alejar la vista del mapa. También está la opción de hacerlo con la rueda del ratón.

.. figure::  images/03_funcionalidades/image3.png
   :align:   center

**2. Medir distancia**

Esta herramienta nos permite medir distancias de un punto a otro.

.. figure::  images/03_funcionalidades/image4.png
   :align:   center

Para medir, hacemos clic en la herramienta y seleccionamos los puntos en el mapa haciendo un clic, para terminar hacemos doble clic.

.. figure::  images/03_funcionalidades/image6.png
   :align:   center

   Ejemplo herramienta medir distancia

**3.Medir áreas**

Esta herramienta nos permite medir áreas.

.. figure::  images/03_funcionalidades/image5.png
   :align:   center


Para medir el perímetro o el área, vamos seleccionando los puntos que conforman el polígono y, cuando vayamos a cerrarlo, hacemos doble clic.
En el caso de que esté activado el mapa urbacatastro también dará el número de UUIIs que abarca el área.

.. figure::  images/03_funcionalidades/image7.png
   :align:   center
   :width: 30%

   Ejemplo herramienta medir área

**4. Escala**

Esta herramienta se encuentra situada en la esquina inferior izquierda y nos muestra una escala que va cambiando según la ampliación del mapa.

.. figure::  images/03_funcionalidades/image8.png
   :align:   center

   Escala

**5. Selector mapa base**

La plataforma dispone de distintos Mapas Base con los que poder trabajar como referencia. Para cambiar los Mapas Base, se debe acceder al botón en forma de mapa del mundo.

.. figure::  images/03_funcionalidades/image9.png
   :align:   center

Actualmente, al cambiar de mapa base, se borran y reinician literalmente todas las capas no vectoriales, añadiendo el basemap nuevo. Esto optimiza el visionado y evita problemas.

Cuando se accede al menú, aparece una nueva ventana con todos los mapas disponibles y una previsualización de los mismos.

.. figure::  images/03_funcionalidades/image10.png
   :align:   center
   :width: 80%

   Listado de Mapas Base

La carga de estos mapas es dinámica, es decir, podremos encontrar más o menos mapas según el proyecto o fecha. Algunos de los mapas por defecto son los siguientes:

* URBA Catastro:
    Recurso propio de aGIS que muestra el catastro urbano estando limitado a uno o dos proyectos.

* Unidades Administrativas:
    Recurso público que muestra las Unidades Administrativas de catastros nacionales.

* Plano impresión:
    Mapa adaptado para la impresión.

* ESRI Carreteras:
    Recurso público de ESRI que muestra la red de carreteras a nivel Global.

* Google Carreteras:
    Mapa vectorial de las carreteras, con etiquetado.

* Google Satélite Hybrid:
    Ortofoto Satélite pública de Google, con etiquetado.

* Google Satélite:
    Ortofoto Satélite pública de Google, sin etiquetado.

* Esri Satélite:
    Recurso público de ESRI que muestra ortofoto a nivel Global.

* CARTO Mapa claro:
    Recurso de CARTO que provee un mapa de carga sencilla, con líneas sencillas, muy recomendable para una visualización básica.

* IGN PNOA:
    Ortofotos de máxima actualidad del IGN.

* IGN Catastro:
    Recurso público del IGN (Instituto Geográfico Nacional) de catastros nacionales.

* IGN Catastro B/N:
    Recurso público del IGN (Instituto Geográfico Nacional) de catastros nacionales en blanco y negro.

* OSM:
    Recurso público y colaborativo de callejeros y catastros. Muy recomendado.


Mi Ubicación
============

Esta herramienta aparece en la parte izquierda de la pantalla y solo será visible cuando estemos en el menú de Mapa.

Al clicar sobre Mi Ubicación, se colocará sobre la ubicación del usuario un marcador y se centrará el mapa sobre esa ubicación ofreciendo sus coordenadas.

.. figure::  images/03_funcionalidades/image11.png
   :align:   center
   :width: 30%

.. figure::  images/03_funcionalidades/image12.png
   :align:   center
   :width: 30%


Ayuda OTDR
==========

Herramienta de apoyo para OTDR. Se selecciona el equipo y se marca la distancia deseada. El resultado muestra un punto en la red a la distancia marcada del la CA.

.. figure::  images/03_funcionalidades/image14_1.png
   :align:   center
   :width: 60%

   Punto en la red que marca la distancia desde la CA


Búsqueda en Red
===============

La pestaña de Búsqueda en Red permite localizar elementos por el campo nombre o bien, en caso de tenerlo, por el campo nombre alternativo. Esta función actúa para capas puntuales y lineales. Las capas, donde se desean hacer las búsquedas las proporcionan la herramienta mediante un menú checkbox.

.. figure::  images/03_funcionalidades/image15.png
   :align:   center
   :width: 30%

Filtros
=======

La pestaña de filtros ayuda al usuario a la hora de encontrar un objeto en específico. Se puede filtrar por:

.. figure::  images/03_funcionalidades/image16.png
   :align:   center
   :width: 30%


* **General**

Podemos seleccionar los distintos elementos que se encuentran en el apartado General (Alimentación, Distribución e Instalación).

.. figure::  images/03_funcionalidades/image17.png
   :align:   center
   :width: 30%


* **Cables**

Podemos seleccionar los distintos elementos que se encuentran en la capa Cables.

.. figure::  images/03_funcionalidades/image18.png
   :align:   center
   :width: 30%


* **Equipos**

Podemos seleccionar los distintos elementos que se encuentran en la capa Equipos.

.. figure::  images/03_funcionalidades/image19.png
   :align:   center
   :width: 30%


* **Capas Radio (APs)**

Podemos seleccionar los distintos elementos que se encuentran en la capa Radio (APs).

.. figure::  images/03_funcionalidades/image20.png
   :align:   center
   :width: 30%


* **Areas de Influencia**

Dividida en tres opciones filtrados combinables entre si --> Area de Despliegue, Area CD (PON) y Areas CA.

.. figure::  images/03_funcionalidades/image21.png
   :align:   center
   :width: 30%


Podemos seleccionar los distintos elementos que se encuentran en la capa Area de Influencia.


* **Canalizaciones y Estructuras**

Dividida en tres opciones. Filtrado por Canalizaciones, filtrado por Estrutuctas o combinación de ambas.

.. figure::  images/03_funcionalidades/image22.png
   :align:   center
   :width: 30%


* **Etiquetas**

Podemos seleccionar los distintos elementos que se encuentran en Etiquetas.

.. figure::  images/03_funcionalidades/image23.png
   :align:   center
   :width: 30%


* **Otras capas**

Dividida en dos capas, Observaciones y UUII parcelas. Opción de filtrado individual o combinación entre ambas.

    *Las UUII Parcela solo aparecerán en el caso de que esté activo el mapa urbaCatastro.

.. figure::  images/03_funcionalidades/image24.png
   :align:   center
   :width: 30%


Capas
=====

aGIS, como Sistema de Información Geográfica, modela los elementos  mediante un sistema de capas que permite la flexibilidad y escalabilidad del sistema.

Sobre el mapa base, se visualizarán estas capas con los distintos elementos, atendiendo a un sistema de visualización.

Las capas son los distintos elementos que se observan a la hora de visualizar la red. Las Capas pueden ser puntuales, lineales o poligonales. También hay un conjunto de capas no geométricas que complementan a las Capas geométricas.

**1. Cables**
----------------
    Capa geométrica y lineal que representa el cableado de red de telecomunicaciones (fibra óptica, cobre, coaxial…).

**2. Equipos**
--------------
    Capa geométrica y puntual que representa el equipamiento de la red.

**3. Central**
--------------
    Capa geométrica y poligonal que representa la central de un proyecto.

**4. OLT**
----------
    Capa geométrica y puntual que representa la cabecera de un proyecto.

**5. Despliegue**
-----------------
    Capa geométrica y poligonal que representa las zonas de despliegue en el proyecto.

**6. Zonas PON**
----------------
    Capa geométrica y poligonal que representa las zonas PON en el proyecto.

**7. Zonas CA**
---------------
    Capa geométrica y poligonal que representa las zonas CA en el proyecto.

**8. Equipos AP**
-----------------
    Capa geométrica y puntual que representa el equipamiento de la red.

**9. Áreas AP**
-----------------
    Capa geométrica y poligonal que representa las diferentes zonas AP.

**10. Canalizaciones**
----------------------
    Capa geométrica y lineal que representa infraestructura lineal como canalización, zanja, pasos aéreos...

**11. Estructuras**
-------------------
    Capa geométrica, y puntual, que representa infraestructura puntual como cámaras, arquetas, postes...

**12. Conductos**
-----------------
    Capa no geométrica, heredada de canalización para representar conductos dentro de arquetas.

**13.Subconductos**
-------------------
    Capa no geométrica, heredada de conducto para representar subconductos dentro de conductos.

**14. Asociaciones**
--------------------
    Capa no geométrica que permite asociar segmentos a canalizaciones mediante el parámetro uuid.

**15. AUX: UUII Parcelas**
--------------------------
    Capa geométrica, y puntual, que representa las UUII.

**16. Observaciones**
---------------------
    Capa geométrica y puntual para indicar anotaciones sobre el mapa.


Diseño
******

.. figure::  images/03_funcionalidades/image25.png
   :align:   center
   :width: 30%

Gestor de Modelos
=================

Es una de las herramientas principales de aGIS, ya que nos permite dibujar en el mapa los distintos elementos disponibles y asignarles los campos necesarios.

La interfaz del gestor de modelos se divide en tres partes:


**Selector de elemento:** Nos permite elegir el elemento que deseemos dibujar en el mapa.

.. figure::  images/03_funcionalidades/image26.png
   :align:   center
   :width: 50%


**Datos generales:** Formulario que dependiendo del elemento seleccionado varía sus campos para una mayor precisión.

.. figure::  images/03_funcionalidades/image27.png
   :align:   center
   :width: 40%

**Datos Específicos:** Formulario opcional que permite que realicemos un seguimiento temporal del elemento, incluso con la opción de añadir observaciones. Contiene estos campos:

    * Fechas de última modificación
    * Fecha de Instalación
    * Fecha de Alta
    * Fecha de Baja
    * Observaciones

.. figure::  images/03_funcionalidades/image28.png
   :align:   center
   :width: 40%

* Dependiendo del tipo de elemento a dibujar los datos expecificos pueden cambiar.

Una vez rellenados los campos necesarios, pulsamos en el botón Dibujar para situar el objeto en el mapa. En el caso de capas lineales o poligonales, para finalizar el trazado, debemos hacer doble clic en el último punto que realicemos.


1. Equipos
----------

.. figure::  images/03_funcionalidades/image29.png
   :align:   center
   :width: 50%

**DATOS GENERALES:**
            **Nombre:**
                Nombre del equipo. * Opción nombre alternativo al marcar "Alternativo".

            **Tipo:**
                Los tipos de equipos disponibles son los siguientes:

                * CA
                * CD
                * T
                * CE
                * P2P

            **Situación:**
                Las posibles situaciones de equipos disponibles son los siguientes:

                * ARQUETA
                * CAMARA
                * FACHADA
                * INTERIOR
                * PEDESTAL
                * POSTEL
                * REGISTRO
                * MASTIL
                * TORRE
                * RACK
                * CASETA

            **Jerarquía (para troncales y subtroncales):**
                * Distribución
                * Alimentación

            **Empalmes:**
                * Número de empalmes.

            **Acometidas:**
                * Número de acometidas.

            **Arrastre (Div MAX):**
                * Número de fibras iluminadas y no iluminadas que admite un determinado equipo con divisor/es.

            **Div Instalados:**
                * Número de Div instalados.

            **Capacidad de Divisores:**
                * Capacidad de splitteo (salidas) de divisores.

            **Equipo Padre:**
                * Asignación de equipo padre (necesario para torpedos con conexiones complejas. Para más información pongase en contacto con soporte).

**DATOS ESPECIFICOS:**

            **Conexión de divisores:**
                - Div in fibra --> in_pos --> SALIDA


2. Cables
---------

.. figure::  images/03_funcionalidades/image30.png
   :align:   center
   :width: 50%

**DATOS GENERALES:**
            **Nombre:**
                Nombre del cable.

            **Tipo:**
                Los tipos de cable disponibles son los siguientes:

                * DI
                * ST
                * T
                * RISER

            **Situación:**
                Las posibles situaciones de equipos disponibles son los siguientes:

                * EXTERIOR
                * CANALIZADO
                * INTERIOR

            **Cubierta:**
                Las posibles cubiertas de equipos disponibles son los siguientes:

                * EST
                * REF

            **Capacidad:**
                * Número de fibras asignadas.

            **Equipo Padre:**
                * Asignación de equipo padre.


3. Central
----------
Forma parte de la entidad de "Cabecera" en el Gestor de Modelos.

.. figure::  images/03_funcionalidades/image31_1.png
   :align:   center
   :width: 50%

.. figure::  images/03_funcionalidades/image31_2.png
   :align:   center
   :width: 50%

**DATOS GENERALES:**

            **Nombre:**
                Nombre de la cabecera.

            **Abreviatura:**
                Abreviatura de la cabecera.

            **Dirección:**
                Dirección de la cabecera.

            **Nº Pach Pannels:**
                Número de Patch Pannels de la Central.

            **Capacidad Pach Pannels:**
                Capacidad de Patch Pannels de la Central.

**DATOS ESPECÍFICOS:**

            **Conexion OLT a PATCHPANNEL:**
                Capacidad de Patch Pannels de la Central.

            **Selección del Patch Pannels de 1 puertos:**

**DATOS DE PUESTA EN MARCHA:**

            **Iniciada: SI/NO**

            **Fecha:**
                Fecha de puesa en marcha.

            **Hora:**
                Hora de puesta en marcha.

            **Observaciones:**



4. OLT's
--------
Forma parte de la entidad de "Cabecera" en el Gestor de Modelos.

.. figure::  images/03_funcionalidades/image31_1.png
   :align:   center
   :width: 50%

.. figure::  images/03_funcionalidades/image31_2.png
   :align:   center
   :width: 50%

**DATOS GENERALES:**

            **Nombre:**
                Nombre de la cabecera.

            **Abreviatura:**
                Abreviatura de la cabecera.

            **Dirección:**
                Dirección de la cabecera.

            **Nº Olts:**
                Número de Olts de la Central.

            **Capacidad Olts:**
                Capacidad de Olts de la Central.

**DATOS ESPECÍFICOS:**

            **Conexion OLT a PATCHPANNEL:**
                Capacidad de Patch Pannels de la Central.

            **Selección del Patch Pannels de 1 puertos:**

**DATOS DE PUESTA EN MARCHA:**

            **Iniciada: SI/NO**

            **Fecha:**
                Fecha de puesa en marcha.

            **Hora:**
                Hora de puesta en marcha.

            **Observaciones:**

5. Despliegue
-------------

.. figure::  images/03_funcionalidades/image32.png
   :align:   center
   :width: 50%

**DATOS GENERALES:**
            **Nombre:**
                Nombre del área.

            **Tipo:**
                Los tipos de cable disponibles son los siguientes:

                * Despliegue
                * Puerto PON
                * Abonado

            **Longitud**
                Longitud de la canalización.

**DATOS UUIIs:**
                * Viviendas.
                * Comercios.
                * Oficinas.
                * Industrias.
                * UUIIs Totales.
                * Observaciones.

6. ZonasPON
-----------

.. figure::  images/03_funcionalidades/image32_1.png
   :align:   center
   :width: 50%

**DATOS GENERALES:**
            **Nombre:**
                Nombre del área.

            **Tipo:**
                Los tipos de cable disponibles son los siguientes:

                * Despliegue
                * Puerto PON
                * Abonado

            **Longitud**
                Longitud de la canalización.

**DATOS UUIIs:**
                * Viviendas.
                * Comercios.
                * Oficinas.
                * Industrias.
                * UUIIs Totales.
                * Observaciones.

7. Zonas CA
-----------

.. figure::  images/03_funcionalidades/image32_2.png
   :align:   center
   :width: 50%

**DATOS GENERALES:**
            **Nombre:**
                Nombre del área.

            **Tipo:**
                Los tipos de cable disponibles son los siguientes:

                * Despliegue
                * Puerto PON
                * Abonado

            **Longitud**
                Longitud de la canalización.

**DATOS UUIIs:**
                * Viviendas.
                * Comercios.
                * Oficinas.
                * Industrias.
                * UUIIs Totales.
                * Observaciones.

8. Observaciones
----------------

.. figure::  images/03_funcionalidades/image33.png
   :align:   center
   :width: 50%

**DATOS GENERALES:**

            **Categoría:**
                * Certificación
                * Edificación
                * Instalación
                * Nota interna
                * Obra Civil
                * Replanteo
                * Área de influencia
                * Carta de empalme

            **Observación**
                Descripción de la observación.


9. AP - Punto de Acceso (Access Point)
--------------------------------------

.. figure::  images/03_funcionalidades/image34.png
   :align:   center
   :width: 50%

**DATOS GENERALES:**
            **Nombre:**
                Nombre de Punto de Acceso.

            **Dirección:**
                Dirección de Punto de Acceso.

            **IP Antena Cliente:**
                Dirección IP de antena cliente.

            **SSID:**
                SSID de Punto de Acceso.

            **PASS SSID:**
                Pass de Punto de Acceso.


10. Área Punto de Acceso (AP)
-----------------------------

.. figure::  images/03_funcionalidades/image35_1.png
   :align:   center
   :width: 50%

**DATOS GENERALES:**
            **Nombre:**
                Nombre del Área Punto de Acceso.

            **BW:**
                Ancho de banda de Punto de Acceso.

            **Observaciones:**
                Descripción de la observación.

**DATOS UUIIs:**
            **Viviendas:**
                Número de viviendas.

            **Comercios:**
                Número de comercios.

            **Oficinas:**
                Número de oficinas.

            **Industrias:**
                Número de industrias.

11. Canalizaciones
------------------

.. figure::  images/03_funcionalidades/image36.png
   :align:   center
   :width: 50%

.. figure::  images/03_funcionalidades/image37.png
   :align:   center
   :width: 50%

**DATOS GENERALES:**

            **Nombre:**
                Nombre de la canalización.

            **Tipo:**
                Indica cómo se realizará la canalización.

                * Canalización
                * Zanja
                * Paso Aéreo

            **Estado:**
                Indicar en qué estado se encuentra la instalación.

                * Proyectado
                * En construcción
                * Instalado
                * Operativo

            **Modalidad:**
                * Alquilado
                * En propiedad

            **SUC:**
                Nombre de la SUC.

            **Estado SUC:**
                Rellenar estado SUC.

            **Dimensión X**

            **Dimensión Y**

            **Anclajes:**
                Número de Anclajes en Canalización.

            **Obturadores Totales:**
                Número de Obturadores Totales en la Canalización.

**DATOS ESPECIFICOS:**
            **Fecha de Última Modificación:**
                * dd/mm/aaaa
            **Fecha de Instalación:**
                * dd/mm/aaaa

            **Fecha de Alta:**
                * dd/mm/aaaa

            **Fecha de Baja:**
                * dd/mm/aaaa

            **Observaciones:**
                Descripción de la observación.


12. Estructura
--------------

.. figure::  images/03_funcionalidades/image38.png
   :align:   center
   :width: 50%

.. figure::  images/03_funcionalidades/image37.png
   :align:   center
   :width: 50%

**DATOS GENERALES:**

            **Nombre:**
                Nombre de la Estructura.

            **Tipo:**
                * Arqueta
                * Cámara de registro
                * Poste
                * Riser
                * Perforación

            **Subtipo:**
                * Genérico
                * Arqueta H
                * Arqueta D
                * Arqueta M
                * Arqueta ICT
                * Arqueta A
                * Arqueta B
                * Arqueta C

            **Material:**
                * Hormigón
                * En madera
                * Fundición

            **Propietario:**
                Nombre del propietario.

            **Modalidad:**

                * Alquilado
                * En Propiedad

            **Estado:**

                * Proyectado
                * En construcción
                * Instalado
                * Operativo

**DATOS ESPECIFICOS:**
            **Fecha de Última Modificación:**
                * dd/mm/aaaa
            **Fecha de Instalación:**
                * dd/mm/aaaa

            **Fecha de Alta:**
                * dd/mm/aaaa

            **Fecha de Baja:**
                * dd/mm/aaaa

            **Observaciones:**
                Descripción de la observación.


Ventanas de Información
=======================

Las ventanas de información se abrirán en la parte derecha de nuestra pantalla cuando se clica en los distintos elementos que se tengan representados en el mapa. Las ventanas de información varían según el elemento que se clica y según los servicios disponibles. En resumen, las ventanas de información desglosan de forma resumida toda la información del objeto que se haya seleccionado.


Mediante las ventanas de información, podremos terminar de configurar los distintos elementos, ver sus esquemas, ver documentos relacionados, obervaciones, incidencias y modificar los datos que se introdujeron cuando se crearon.


.. figure::  images/03_funcionalidades/image39_1.png
   :align:   center
   :width: 50%

.. figure::  images/03_funcionalidades/image39_2.png
   :align:   center
   :width: 50%

.. figure::  images/03_funcionalidades/image40.png
   :align:   center
   :width: 50%

Como podemos observar en la imagen las interfaces están compuestas  por:

    **Nombre del elemento:**
        Es el nombre que establecemos al crear el elemento.

    **Elemento:**
        Tipo de elemento.

    **Datos Generales:**
        Contiene los datos generales de creación de un tipo de elemento en concreto.

    **Esquema:**
        Nos proporciona diferentes esquemas.

    **Trabajos:**
        Nos muestra los diferentes trabajos asignados al elemento.

    **Incidencias:**
        Nos muestra las diferentes incidencias asignados al elemento.

    **Datos Específicos:**
        Contiene datos concretos sobre la creación (fechas y observaciones).

    **Documentos:**
        Nos muestra los diferentes documentos asignados al elemento desde la utilidad Gestor Documental.

    **Observaciones:**
        Nos muestra las diferentes incidencias asignadas al elemento.


Además de estas pestañas, disponemos de tres botones en la parte superior derecha.


.. |image41| image:: images/03_funcionalidades/image41.png
    :width: 40

.. |image42| image:: images/03_funcionalidades/image42.png
    :width: 20

.. |image43| image:: images/03_funcionalidades/image43.png
    :width: 30

**Modificar** |image41|

    Para modificar un elemento, se debe presionar sobre el icono del lápiz y editar los datos generales o específicos. Desde esta ventana, podemos eliminar un elemento pulsando el botón eliminar.  Dependiendo del elemento, se habilitarán diferentes opciones concretas que analizaremos más adelante.


**Expandir/minimizar** |image42|

    Permite la minimización y expansión de la ventana de información.


**Cerrar** |image43|

    Mediante este botón podemos cerrar la ventana de información.


Vamos a analizar las opciones más concretas que poseen los distintos elementos:

a. Modificación del trazado
---------------------------
Los cables, las canalizaciones y líneas permiten la modificación de su trazado con la opción de Modificar.

Para ello, debemos estar en el modo de edición de uno de los elementos antes mencionados.


.. figure::  images/03_funcionalidades/image44.png
   :align:   center
   :width: 80%

Como vemos en la captura el trazado aparece en color naranja y se marcan un punto de color rojo en cada extremo y uno central.

Desde los extremos, podemos extender, encoger y mover la posición del punto.

.. figure::  images/03_funcionalidades/image45.png
   :align:   center
   :width: 80%

Como vemos en la imagen, hemos desplazado el trazado del segmento.

Desde el cuadrado central, podemos añadir otro vértice al trazado.

.. figure::  images/03_funcionalidades/image46.png
   :align:   center
   :width: 80%

Entre cada vértice, se creará un cuadrado intermedio para poder editar.


b. Canalizaciones
-----------------

Desde la ventana de canalizaciones, podemos añadir conductos y subconductos de una manera sencilla.

.. figure::  images/03_funcionalidades/image47.png
   :align:   center
   :width: 40%


La canalización vacía se representa de la siguiente forma:


.. figure::  images/03_funcionalidades/image47_1.png
   :align:   center
   :width: 30%

.. |image49| image:: images/03_funcionalidades/image49.png
    :width: 80

**i. Añadir conducto**
    Para ello, desplegamos la pestaña conductos y subconductos. En la parte de conductos, seleccionamos las opciones que deseemos y le damos al botón |image49|

    Una vez hecho esto, nos aparecerá a la derecha de Conductos COND 1, si lo seleccionamos

.. figure::  images/03_funcionalidades/image50.png
   :align:   center
   :width: 30%

   Ahí aparecerán los conductos que vayamos creando.


.. figure::  images/03_funcionalidades/image51.png
   :align:   center
   :width: 30%

   Representación de un conducto


.. |image52| image:: images/03_funcionalidades/image52.png
    :width: 80


**ii. Eliminar conducto**
    Para eliminar un conducto, tan solo debemos seleccionarlo haciendo clic en el COND que queramos eliminar y darle al botón |image52|.


.. |image53| image:: images/03_funcionalidades/image53.png
    :width: 100

**iii. Añadir subconducto**
    Para añadir un subconducto, primero, debemos seleccionar un conducto. Una vez seleccionado el COND, elegimos las opciones que deseemos y le damos al botón |image53|.


    Una vez hecho, esto nos aparecerá a la derecha de Subconductos SUBCOND 1

.. figure::  images/03_funcionalidades/image54.png
   :align:   center
   :width: 30%

   Ahí aparecerán los subconductos que vayamos creando, según el conducto preseleccionado.



.. figure::  images/03_funcionalidades/image55.png
   :align:   center
   :width: 30%

   Representación de un subconducto

.. |image56| image:: images/03_funcionalidades/image56.png
    :width: 100

**iv. Eliminar subconducto**
    Para eliminar un subconducto, tan solo debemos seleccionarlo haciendo clic en el SUBCOND que queramos eliminar y darle al botón |image56|.

**v. Esquema**
    Una vez realizados los conductos y subconductos, podemos hacer un zoom para ver el esquema de la canalización. Sólo hará falta hacer clic encima del subconducto.

.. figure::  images/03_funcionalidades/image57.png
   :align:   center
   :width: 30%

   Vista general de canalización

.. figure::  images/03_funcionalidades/image57_1.png
   :align:   center
   :width: 30%

   Vista ampliada de canalización


c. Equipos
----------

Desde la ventana de equipos, podemos añadir Divisores.

.. figure::  images/03_funcionalidades/image58.png
   :align:   center
   :width: 40%

.. |image59| image:: images/03_funcionalidades/image59.png
    :width: 100

.. |image60| image:: images/03_funcionalidades/image60.png
    :width: 190

Para ello, una vez activado el modo edición, en la pestaña de Datos Geberales en el apartado de "Div Instalados" seleccionamos el número de Divisores que deseamos (botón "+") |image59|.

Después, en "Capacidad de divisores" seleccionamos la capacidad que deseemos |image60|.

Una vez hecho esto, al guardar, aparecerá el Númedo de Divisores.

.. figure::  images/03_funcionalidades/image62.png
   :align:   center
   :width: 50%

   Apareceren los divisores que vamos creando

   Para eliminar un Divisor volvemos a activar el modo edición y en el apartado de "Div Instalados" le damos a "-" para eliminar el último Divisor creado |image59|.

.. figure::  images/03_funcionalidades/image63.png
   :align:   center
   :width: 40%

   Esquema del Divisor


d. Exportación de esquemas
--------------------------

Los esquemas de los diferentes elementos se pueden exportar en formato excel.

.. |image68| image:: images/03_funcionalidades/image68.png
    :width: 25

Simplemente, debemos hacer clic en el botón de exportar |image68|


.. figure::  images/03_funcionalidades/image69_1.png
   :align:   center
   :width: 60%

   Esquema de exportación

.. figure::  images/03_funcionalidades/image70_1.png
   :align:   center
   :width: 60%

   Excel de exportación


e. Iluminación
--------------

La iluminación nos permite resaltar el tramo correspondiente a un cable. Simplemente, pulsamos sobre el cable.

.. figure::  images/03_funcionalidades/image71_1.png
   :align:   center
   :width: 80%


Tabla de Atributos
==================

La tabla de atributos muestra  las distintas capas del mapa.

.. figure::  images/03_funcionalidades/image72.png
   :align:   center
   :width: 100%

   Vista General Tabla de Atributos

.. |image73| image:: images/03_funcionalidades/image73.png
    :width: 25

.. |image74| image:: images/03_funcionalidades/image74.png
    :width: 25

.. |image75| image:: images/03_funcionalidades/image75.png
    :width: 25

.. |image76| image:: images/03_funcionalidades/image76.png
    :width: 25

.. |image77| image:: images/03_funcionalidades/image77.png
    :width: 25

.. |image78| image:: images/03_funcionalidades/image78.png
    :width: 25

.. |image79| image:: images/03_funcionalidades/image79.png
    :width: 25

.. |image64| image:: images/03_funcionalidades/image64.png
    :width: 25

.. |image65| image:: images/03_funcionalidades/image65.png
    :width: 25

.. |image66| image:: images/03_funcionalidades/image66.png
    :width: 25

.. |image67| image:: images/03_funcionalidades/image67.png
    :width: 25


*   |image65| Mostrar en el mapa el elemento seleccionado.

*   |image73| Borrar el elemento seleccionado.

*   |image74| Restaurar datos iniciales.

*   |image75| Guardar cambios.

*   |image76| Exportar a .CSV.

*   |image77| Exportar a .SHP.

*   |image78| Exportar a .KML.

*   |image79| Exportar a .GPKG.

*   |image66| Buscar elemento.

*   |image67| Informe fibra central.

*   |image64| Mostrar barra de formulas.


Gestor de Conexión
==================

1. Conexiones
-------------

El gestor de conexiones nos permite conectar cables con cables.

Algo a tener en cuenta es que para que, un cable esté conectado a otro cable, deberemos entrar en el modo de edición del cable y pegarlo cerca del cable al que se le quiere hacer la conexión, como vemos en la siguiente captura:

.. figure::  images/03_funcionalidades/image81.png
   :align:   center
   :width: 80%

Una vez posicionado correctamente, le damos a guardar para confirmar los cambios.

Para usar la herramienta gestor de conexiones, hacemos clic en Gestor Conexiones, se nos abrirá la siguiente interfaz:

.. figure::  images/03_funcionalidades/image82.png
   :align:   center
   :width: 55%

Como vemos en la imagen, la casilla origen aparece marcada en amarillo, por lo que debemos seleccionar el origen de la conexion. En el ejemplo, seleccionaremos al cable haciendo clic sobre él.

.. figure::  images/03_funcionalidades/image80.png
   :align:   center
   :width: 60%

Una vez seleccionado el cable, aparecerán las conexiones posibles que anteriormente habremos configurado en el mismo. Ahora, debemos hacer clic en la casilla Destino para que se ponga en amarillo y, posteriormente, seleccionamos el destino haciendo clic en él, sobre el mapa. Una vez hecho, se cargan sus posibles conexiones.

.. figure::  images/03_funcionalidades/image83.png
   :align:   center
   :width: 40%

   Origen seleccionado

.. figure::  images/03_funcionalidades/image84.png
   :align:   center
   :width: 40%

   Destino seleccionado

Para realizar la conexión, debemos seleccionar las conexiones que queremos que se unan, para seleccionar varias al mismo tiempo, dejaremos pulsada la tecla CTRL mientras hacemos clic en ellas.

Hecho esto le daremos al botón verde para unirlas o al botón rojo si queremos eliminarlas.

.. figure::  images/03_funcionalidades/image85_1.png
   :align:   center
   :width: 40%

   Antes de la conexión

.. figure::  images/03_funcionalidades/image86_1.png
   :align:   center
   :width: 45%

   Después de la conexión

Cuando realizamos conexiones, éstas se ven reflejadas en las ventanas de información de los elementos implicados haciendo clic sobre el.

.. figure::  images/03_funcionalidades/image88.png
   :align:   center
   :width: 80%

   Esquema de cable

Con lo que hemos realizado tan solo hemos conectado uno de los extremos del cable, por lo tanto, debemos realizar el mismo proceso desde el otro extremo. Digamos que cuando seleccionamos un elemento desde la casilla de ORIGEN, estamos seleccionando un extremo, y si seleccionamos el mismo elemento desde la casilla DESTINO, estamos seleccionando el otro extremo. Podemos verlo más sencillo con el siguiente esquema:

.. figure::  images/03_funcionalidades/image89.png
   :align:   center
   :width: 70%

En el dibujo, se muestran el origen (morado) y el destino (naranja) de los diferentes elementos. El rectángulo verde, nos muestra lo que hemos configurado. Para llegar al equipo Y, debemos seleccionar en la casilla ORIGEN el tramo X y en la casilla DESTINO el equipo Y, realizamos las conexiones que deseemos y ya tendríamos conectado el equipo X al equipo Y.

2. Nomenclatura de Conexiones
-----------------------------
**Cabecera de Proyecto (OLT)**

*   **Nombre**

*   **Abreviatura** -- Obligatoria, pues de ella dependerá parte de la nomenclatura de la red.

**Niveles de Red**

*   **Trocales** -- Desde la OLT a los distintos Torpedos. Los torpedos se usan para “ramificar” las fibras que salen de la central, y delimitan la red troncal de la subtroncal. La red troncal estará formada por cables de un alto número de fibras (Desde central a CDs).

*   **Subtrocales** -- Se define como tramo de la red que va desde los Torpedos a la última caja de distribución/derivación (Desde central a CDs).

*   **Distribución** -- Se define como tramos que van desde una caja de abonado, hasta las cajas de abonado o CTOs. La red de distribución ya han sido spliteada en la caja de distribución (Desde CDs a CAs).

**Etiquetado**

.. figure::  images/03_funcionalidades/image121.png
   :align:   center
   :width: 80%

   Topología de la red

   Ejemplo de diseño y nomenclatura de red

.. figure::  images/03_funcionalidades/image122.png
   :align:   center
   :width: 80%

   Ejemplo arrastre

Siguiendo el ejemplo de la imagen de abajo, a la hora de diseñar la red habrá que seguir un patrón común de etiquetado.

    - Antes de nada aclarar que la regla principal del etiquetado en aGIS es que se va heredando los nombres desde la cabecera.
    - El etiquetado deben tener una línea circular.

Descripción:

    Desde la cabecera (OLT) saldrá una troncal llamada T.A (Troncal A) que continúa hasta el torpedo TA. El siguiente tramo de troncal se llamará T.B (Troncal B) e irá hasta TB (torpedo B), y así sucesivamente.

    Del torpedo TA saldrán, en este caso, las ramas subtroncales, llamadas ST.A.1 y ST.A.2, etc.

    Siguiendo la ST.A.1 nos encontramos con la CD A.1 (marcando el fin de ST A.1 y comenzando la ST A.2 hasta la siguiente caja de distribución llamada CD A.2), la cual contiene una ramificación de cables de distribución que se nombrarán DI A1.1, DI A1.2, DI A1.3, etc.

    El cable DI A1.1 conecta con la caja de abonado llamada CA A.1.1 y esa a su vez conecta con DI A1.2 que sigue hasta CA A1.2, y así hasta completarlo todo.

.. figure::  images/03_funcionalidades/image124_1_1.png
   :align:   center
   :width: 90%

   Ejemplo de etiquetado en aGIS


Gestor de Asociaciones
======================

Esta herramienta nos permite atribuir cables a canalizaciones.

Para ello, abrimos la herramienta y hacemos clic primero en el segmento y luego en la canalización. Ahora hacemos clic en el conducto por el que queremos que pase nuestro cable y le damos al botón verde para aplicar o al botón rojo para eliminar.

.. figure::  images/03_funcionalidades/image91.png
   :align:   center
   :width: 50%

Esta información se puede comprobar de nuevo yendo al Esquema de la canalización, donde al igual que en el gestor de asociaciones aparecerá en blanco por donde pase un cable.


Herramientas
============
Este apartado se compone de dos herramientas principales, cortar cable y cortar canal.

.. figure::  images/03_funcionalidades/image92.png
   :align:   center
   :width: 50%

Cortar cables y canalizaciones
------------------------------

Usaremos de ejemplo la herramienta cortar cable, ya que la herramienta de cortar canal funciona igual.

Para cortar un cable debemos usar esta herramienta. Simplemente, pulsamos el botón y seleccionamos, como podemos ver en la captura, dos puntos: el primero con un clic y el segundo con doble clic.

.. figure::  images/03_funcionalidades/image95.png
   :align:   center
   :width: 80%

   Cortar cable

.. figure::  images/03_funcionalidades/image96.png
   :align:   center
   :width: 80%

   Cable cortado izquierda

.. figure::  images/03_funcionalidades/image97.png
   :align:   center
   :width: 80%

   Cable cortado derecha

Ahora tenemos dos cables que se llaman igual y tienen las mismas características, sin embargo, las conexiones permanecen en los extremos por los que vienen.


Editor de Estilos CSS
=====================

Esta herramienta nos permite la personalización de elementos como: cables, equipos, canalizaciones e infraestructuras pudiendo cambiar desde colores hasta iconos y asignarle condiciones.

La configuración se realiza mediante un sistema de reglas CSS y, posteriormente, mediante un sistema de condicionales en caso de que quiera darse ese estilo a un conjunto definido de elementos. Dichas reglas se guardan en el proyecto.

En caso de querer añadir estilos, tendremos que añadir toda la casuística necesaria ya que el estilo por defecto se desactiva totalmente.

Cada uno de los elementos tiene una pestaña diferente pero la interfaz es prácticamente la misma por lo que vamos a comprobar su funcionamiento en la pestaña Equipos.

Al entrar, se nos muestra una previsualización del mapa, una tabla con las distintas personalizaciones y un botón de "Nueva Regla".

.. figure::  images/03_funcionalidades/image106.png
   :align:   center
   :width: 90%

   Vista general estilos

Para añadir una personalización nueva le damos al botón Nueva Regla y rellenamos el formulario que aparece como deseemos.

Se puede modificar el símbolo, el tamaño del símbolo,  la opacidad del símbolo, color de relleno, opacidad de relleno, color de borde, ancho de borde y opacidad de borde:

.. figure::  images/03_funcionalidades/image107.png
   :align:   center
   :width: 80%

   Configuración de CSS

.. figure::  images/03_funcionalidades/image108.png
   :align:   center
   :width: 100%

   Editor CSS

.. |image109| image:: images/03_funcionalidades/image109.png
    :width: 25

Para que funcione debemos asignarle una condición pulsando en |image109|

.. figure::  images/03_funcionalidades/image104.png
   :align:   center
   :width: 80%

La  condición depende del elemento al que estemos asignando, en el ejemplo lo que hemos configurado es que los equipos que tengan como situación FACHADA se les aplique el estilo.

.. figure::  images/03_funcionalidades/image105.png
   :align:   center
   :width: 100%

.. |image110| image:: images/03_funcionalidades/image110.png
    :width: 70

.. |image111| image:: images/03_funcionalidades/image111.png
    :width: 50

Se pueden asignar varias condiciones o subcondiciones a un mismo estilo. Para volver usar el estilo por defecto debemos eliminar los estilos personalizados o editarlos con el botones |image110|.
Lo mismo ocurriría con las subcondiciones |image111|.

El usuario podrá exportar la configuración de estilos que haya personalizado. O bien, importar una ya predefinida mediante los botones "Importar/Exportar".

.. figure::  images/03_funcionalidades/image105_1.png
   :align:   center
   :width: 100%

   Exportación / Importación de estilos


Modelos de Cables
=================

Esta opción nos permite crear un código de colores de un fabricante.

.. figure::  images/03_funcionalidades/image114.png
   :align:   center
   :width: 80%

   Vista general modelo cables

Para crear un nuevo cable le damos a "Nuevo Modelo", esto permite crear tantos modelos como necesitemos. Para crear un código de colores, hay que tener claro una serie de campos:

.. figure::  images/03_funcionalidades/image115.png
   :align:   center
   :width: 80%

* NOMBRE DEL FABRICANTE:
    Identificador que, posteriormente, asignaremos al cable para la asignación de su código.

* FIBRAS POR TUBO:
    Módulo del cable, es decir, fibras por tubo, esto asignará tantos colores como fibras por tubo, que serán los mismos para cada conjunto de fibras por tubo.

* MAX TUBO:
    Número máximo de tubos que admite nuestro código de colores. Esto asignará tantos colores como tubos máximos tengamos en el cable.

* MODO 2 Colores:
    Permite que tanto tubos como fibras, admitan varios colores.

* MODO Color Interior-Exterior:
    Permite diferenciar color interior de exterior.

* Habilitar Marcas Negras:
    Permite marcas negras.

* Anillos Interior-Exterior en cable:
    Permite visualizar los anillos del cable.

.. |image116| image:: images/03_funcionalidades/image116.png
    :width: 25

Una vez completamos los datos le damos al botón de editar (|image116|) para configurar el código de colores.

La configuración de colores es sencilla, en columna acción, tan solo hay que pulsar el botón de editar y entramos en el panel de colores.

.. figure::  images/03_funcionalidades/image117.png
   :align:   center
   :width: 80%

Por defecto, el valor es NULL, aunque aparezca en blanco, por lo que se  asigna colores pulsando sobre el círculo de la columna COLOR tanto para fibras como para tubos.

El icono de la brocha, sirve para copiar y pegar un color.
Brocha - Círculo a Copiar - Círculo a Pegar

Una vez asignados todos los colores, se puede proceder a asignarlos a un cable.

.. figure::  images/03_funcionalidades/image118.png
   :align:   center
   :width: 80%

Para asignar un color a un cable, se debe de seleccionar cable en cuestión, entrar al modo edición, y en el campo de Fabricante se introducirá el nombre del Fabricante que queramos asignar respectivamente. Por ejemplo, Default.

.. figure::  images/03_funcionalidades/image119.png
   :align:   center
   :width: 80%

Al dar a Guardar, y acceder a su infowindows o ventana de información, ya se verá el código de colores, según lo que se haya asignado.

.. figure::  images/03_funcionalidades/image120.png
   :align:   center

La columna de la izquierda representa el color del tubo, y la columna de la derecha el color de la fibra.
