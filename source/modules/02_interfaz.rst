#################################
Interfaz General de la Aplicación
#################################


Una vez logueados, aGIS presenta una interfaz minimalista y sencilla, con un look&feel moderno, dinámico y optimizado para realizar las distintas tareas de forma sencilla. Se ha de tener en cuenta que la interfaz puede sufrir pequeñas variaciones entre las distintas versiones de navegadores compatibles.

.. figure::  images/02_interfaz/image1_1.png
   :align:  center
   :width: 80%

   Mapa aGIS

Secciones de la Interfaz
************************

En la interfaz principal, se puede apreciar tres secciones bien diferenciadas: en la parte superior, atravesando la ventana, el Header; a la izquierda, el menú; y en el resto de la interfaz, el panel de Interacción que, por defecto, muestra el Dashboard.

.. figure::  images/02_interfaz/image2.png
   :align:  center
   :width: 80%

   Dashboard aGIS

Header
======

En la sección del Header, a la derecha,  se encuentra el botón para seleccionar un proyecto asignado, y a la izquierda, el botón de minimizado del menú lateral. Por último, encontramos el botón de desconexión (“Log Out”).

.. figure::  images/02_interfaz/image3.png
   :align:  center
   :width: 80%

Desde la vista del mapa aparece en el header un buscador de direcciones, que nos proporciona la utilidad de encontrar una ubicación concreta del mapa usando las coordenadas (en grados decimales. Ej:40.437368, -3.709672) o una dirección.


.. figure::  images/02_interfaz/image4.png
   :align:  center
   :width: 80%

   Coordenadas en grados decimales

.. figure::  images/02_interfaz/image5.png
   :align:  center
   :width: 80%

   Dirección

Este buscador de direcciones incluye un buscador de cobertura, se abrirá una ventana emergente llamada "Detalle de Búsqueda" donde se nos mostrará:
    - Cobertura
    - Estado
    - Calle
    - Cobertura disponibilidad
    - Ancho de Banda

.. figure::  images/02_interfaz/image5_1.png
   :align:  center
   :width: 60%


Menú
====

En la sección del menú, encontramos los distintos apartados en los cuales se divide la plataforma, estos apartados serán explicados en profundidad posteriormente. Además, dichos apartados aparecerán o desaparecerán según el grado de permisos de los que se disponga, así, como del proyecto que esté activo. Por ejemplo, el mapa mostrará las herramientas de Edición, mientras que el Dashboard no.

.. figure::  images/02_interfaz/image6.png
   :align:   center
   :width: 30%


Dashboard
=========
En el Dashboard, se encuentra toda la información centralizada. Es la pantalla principal cuando entramos a la sesión.

.. figure::  images/02_interfaz/image7.png
   :align:   center
   :width: 80%

Como se observa en la captura, el panel es muy conciso y claro. Podemos apreciar tres columnas.

En la primera se encuentra el nombre del proyecto seleccionado.

En la columna central, se encuentra la vista rápida de notificaciones y video tutoriales. Si hacemos clic podremos ver en detalle cada notificación.

La última columna se divide en tres ventanas, la primera da información de los items del proyecto .La segunda nos da información genérica de aGIS FTTH (dirección, dirección web, correo electrónico, teléfono de contacto...). La tercera ventana da acceso a este manual.

.. figure::  images/02_interfaz/image8.png
   :align:   center
   :width: 30%


Perfil
======

De forma suplementaria, pulsando donde aparece nuestro alias y la organización, se desplegará un menú donde apareceran los apartados del perfil.Destacar que la mayoría de los apartados de perfil solo estarán disponibles para el administrador de la organización.

.. figure::  images/02_interfaz/image9.png
   :align:   center
   :width: 30%

   Desplegable de perfil

.. figure::  images/02_interfaz/image10.png
   :align:  center
   :width: 80%

   Menú del perfil


Una vez dentro de perfil se pueden administrar todos los datos de nuestro usuario, o toda la información y gestión correspondiente a la área, los proyectos y los permisos. Se encuentra dividida en cinco pestañas: Organización, Gestión de usuarios, Gestión de permisos, Consultar accesos, Tokens, etc...

Cada pestaña pertenece a un apartado del perfil, el cual mostrará los datos y configuraciones disponibles.


Organización
------------

Datos relativos a la organización o compañía de la cuenta. Modificable por el administrador de la organización.

- Nombre: Nombre de la organización.
- Razón Social.
- E-Mail: Mail de Notificaciones de la organización.
- Teléfono: Teléfono de contacto de la organización.
- Avatar: Logo usado por la organización.

.. figure::  images/02_interfaz/image11.png
   :align:   center
   :width: 80%

   Vista menú Organización

Gestión de usuarios
-------------------
Muestra la información de usuarios de la organización (nombre, alias, mail, teléfono y tipo de usuario). Sólo disponible en caso de ser administrador de compañía.

.. figure::  images/02_interfaz/image12.png
   :align:   center
   :width: 80%

   Vista menú Gestión de usuarios

.. |image12_2| image:: images/02_interfaz/image12_2.png
    :width: 30

En este apartado el administrador de la organización puede cambiar las contraseña de los ususarios de su organización mediante el botón |image12_2| de la columna "Actions".

.. figure::  images/02_interfaz/image12_1.png
   :align:   center
   :width: 80%

   Cambio de la pass del usuario

Gestión de Proyectos
--------------------
Muestra la información de proyectos de la organización (nombre, tipo y acciones disponibles). Sólo disponible en caso de ser administrador de compañía.

.. figure::  images/02_interfaz/image16.png
   :align:   center
   :width: 80%

   Vista menú Gestión de Proyectos

En la columna de "Acción" se podrá realizar carga de archivos e importaciones y exportaciones de estilos.

.. figure::  images/02_interfaz/image16_1.png
   :align:   center
   :width: 80%


Consultar Despliegue UUII
-------------------------
Muestra la información de despliegue de las UUII. Sólo disponible en caso de ser administrador de compañía.

.. figure::  images/02_interfaz/image17.png
   :align:   center
   :width: 80%

   Vista menú Despliegue UUII


Consultar UUII Zonas AI
-----------------------
Muestra la información de las UUII de las zonas AI. Sólo disponible en caso de ser administrador de compañía.

.. figure::  images/02_interfaz/image18.png
   :align:   center
   :width: 80%

   Vista menú UUII Zonas AI


Gestión de permisos
-------------------

Permite a los administradores de la compañía, modificar los proyectos y los permisos de los usuarios de su compañia. Sólo disponible en caso de ser administrador de compañía.

.. figure::  images/02_interfaz/image13.png
   :align:   center
   :width: 80%

   Vista menú Gestión de permisos

**Consulta de Permisos**

Para consultar los proyectos asignados de un usuario, de la captura anterior en gestión de permisos, en el apartado "Ver Proyectos y Usuarios" seleccionamos el usuario. A continuación se nos mostrará una lista donde aparecerán todos los proyectos asiganos a ese usuario.

Para consultar un proyecto y todos sus usuarios asignados sería de la misma manera pero seleccionando en este caso el Proyecto.

.. figure::  images/02_interfaz/image13_1.png
   :align:   center
   :width: 80%

   Ejemplo consulta de proyectos asigandos de un usuario

.. figure::  images/02_interfaz/image13_2.png
   :align:   center
   :width: 80%

   Ejemplo consulta de usuarios asigandos a un proyecto

**Asignar Proyectos y Usuarios**

En el apartado de "Asignar Proyectos y Usuarios" para asignar proyectos a un usuario en primer lugar tendremos que seleccioanr el usuario/s de la colimna "Non-selected" y pasarlos a la columna "Selected" mediante las flechas que aparecen. Seguidamente en la sección "Seleccionar Proyecto" elegimos el proyecto a asignar y clicmaos en el botón "vincular proyecto". Este mismo proceso se realiza para desvincular, clicando en este caso en el botón "desvincular proyecto".

En cuanto al apartado "Permisos" funcionaría de manera igual, dando en este caso en el botón "aplicar" para asignar los permisos.

.. figure::  images/02_interfaz/image13_3.png
   :align:   center
   :width: 80%

   Ejemplo vinculación proyectos/permisos


Consultar accesos
-----------------
Permite a los administradores de la compañía, consultar los accesos de los usuarios de su compañia. Se crea una tabla con la fecha, proyecto, actividad y usuario. Sólo disponible en caso de ser administrador de compañía.

.. figure::  images/02_interfaz/image14.png
   :align:   center
   :width: 80%

   Vista menú accesos


Notificaciones
--------------
Permite a los administradores de la compañía, activar o desactivar notificaciones de cambios de estado en OLT, PON o ONT. Sólo disponible en caso de ser administrador de compañía.

.. figure::  images/02_interfaz/image19.png
   :align:   center
   :width: 80%

   Vista menú Notificaciones


Tokens
------
Permite a los usuarios consultar su nombre de usuario y su tokens.

.. figure::  images/02_interfaz/image15.png
   :align:   center
   :width: 80%

   Tokens

Log out
-------
Para cerrar sesión pulsamos en "Salir".

.. figure::  images/02_interfaz/image20.png
   :align:   center
   :width: 20%

   Log Out