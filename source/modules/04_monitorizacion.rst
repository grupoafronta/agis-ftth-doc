##############
Monitorización
##############

Desde este menú podremos acceder a la gestión de abonados, equipos activos, configuración de filtros de áreas de influencia y la configuración de OLTs y PONs de nuestro proyecto.


.. figure::  images/04_monitorizacion/image1.png
   :align:   center
   :width: 30%

   Menú Monitorización

.. |image2| image:: images/04_monitorizacion/image2.png
    :width: 80

Desde el botón |image2| podremos actualizar la monitorización del mapa.

En el apartado de "Gestión de abonados" se nos mostrará una tabla donde se listan los usuarios abonados, dando información de dichos abonados.

.. figure::  images/04_monitorizacion/image4.png
   :align:   center
   :width: 80%

   Lista de abonados

Para añadir un nuevo Cliente se ha de clciar en el botón "Registrar nuevo Cliente". Se abrirá un formulario que deberemos rellenar.

.. figure::  images/04_monitorizacion/image3.png
   :align:   center
   :width: 80%

   Formulario nuevo cliente


Pasando a la "Gestión de Equipos Activos" se muestra una tabla indicando los equipos registrados. La tabla muestra información relativa al nombre, IP, coordenadas XY, icono, estado, observaciones y la edición o borrado de equipo.

.. figure::  images/04_monitorizacion/image5.png
   :align:   center
   :width: 80%

   Lista de Equipos Activos

Para añadir un nuevo equipo, mediante el botón "Registrar nuevo Equipo Activo", se nos abrirá el formulario donde tendremos que introducir los datos necesarios para su registro.

.. figure::  images/04_monitorizacion/image6.png
   :align:   center
   :width: 80%

   Formulario de registro de nuevo equipo activo

